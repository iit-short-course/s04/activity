from abc import ABC, abstractmethod
class Animal:
	@abstractmethod
	def eat(self, food):
		pass
	def make_sound(self):
		pass

class Cat(Animal):
	def __init__(self, name, breed, age):
		self.name = name
		self.breed = breed
		self.age = age 


class Dog(Animal):
	def __init__(self, name, breed, age):
		self.name = name
		self.breed = breed
		self.age = age 


class Geeks:
     def __init__(self):
          self._age = 0
       
     # using property decorator
     # a getter function
     @property
     def age(self):
         print("getter method called")
         return self._age
       
       
mark = Geeks()
  
mark.age = 19
  
print(mark.age)
